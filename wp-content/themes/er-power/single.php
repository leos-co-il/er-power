<?php

the_post();
get_header();
$fields = get_fields();
?>


<article class="page-body post-body">
	<div class="container">
		<div class="row justify-content-between align-items-stretch mt-5">
			<div class="col-12">
				<h1 class="post-title-page"><?php the_title(); ?></h1>
			</div>
			<div class="col-lg-6 col-12 col-content">
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-lg-6 col-12 fixed-form">
				<?php if (has_post_thumbnail()) : ?>
					<img src="<?= postThumb(); ?>" alt="post-image" class="post-image-main">
				<?php endif; ?>
				<div class="post-form-wrap">
					<?php if ($title_f = opt('post_form_title')) : ?>
						<h2 class="form-title"><?= $title_f; ?></h2>
					<?php endif;
					if ($subtitle_f = opt('post_form_text')) : ?>
						<p class="form-subtitle mb-3"><?= $subtitle_f; ?></p>
					<?php endif; ?>
					<?php getForm('17'); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<div class="form-search-back">
	<?php get_template_part('views/partials/repeat', 'search'); ?>
</div>
<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'text' => $fields['faq_text'],
			'faq' => $fields['faq_item'],
		]);
endif;
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 3,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="posts-output pt-pb">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="base-output block-text">
						<?= $fields['same_posts_text'] ? $fields['same_posts_text'] : '<h2>מאמרים נוספים שיכולים לעניין אותך</h2>'; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<?php foreach ($samePosts as $x => $post) {
					get_template_part('/views/partials/card', 'post', [
						'post' => $post,
					]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>


