<?php
$contact_id = getPageByTemplate('views/contact.php');
$current_id = get_the_ID();
$facebook = opt('facebook');
$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$post = opt('post_address');
?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png" alt="to-top">
			<span class="top-text">חזרה למעלה</span>
		</a>
		<?php if ($current_id !== $contact_id) : ?>
			<div class="repeat-form mb-5">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="foo-form-wrap">
								<div class="row align-items-center justify-content-center mb-3">
									<?php if ($foo_title = opt('foo_form_title')) : ?>
									<div class="col-auto">
										<h2 class="form-title">
											<?= $foo_title; ?>
										</h2>
									</div>
									<?php endif;
									if ($foo_subtitle = opt('foo_form_subtitle')) : ?>
									<div class="col-auto">
										<h2 class="form-subtitle">
											<?= $foo_subtitle; ?>
										</h2>
									</div>
									<?php endif; ?>
								</div>
								<?php getForm('16'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="container">
			<div class="row justify-content-between foo-row">
				<div class="col-xl-3 col-lg-auto col-sm col-6 foo-menu main-footer-menu">
					<h3 class="foo-title">תפריט עזר</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '2', '',
								'main_menu h-100 text-right'); ?>
					</div>
				</div>
				<div class="col-sm col-12 foo-menu links-footer-menu">
					<h3 class="foo-title">ביטויים לקידום</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '2', 'hop-hey four-columns'); ?>
					</div>
				</div>
				<div class="col-lg-auto col-sm col-6 foo-menu contacts-footer-menu">
					<h3 class="foo-title">פרטי התקשרות</h3>
					<div class="menu-border-top">
						<span class="snif-title">סניף ראשי</span>
						<ul class="contact-foo-list">
							<?php if ($address) : ?>
								<li>
									<a href="https://www.waze.com/ul?q=<?= $address; ?>" class="contact-info">
										<?= $address; ?>
									</a>
								</li>
							<?php endif;
							if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info">
										<?= 'טל: '.$tel; ?>
									</a>
								</li>
							<?php endif;
							if ($fax) : ?>
								<li>
								<span class="contact-info">
								<?= 'פקס: '.$fax; ?>
							</span>
								</li>
							<?php endif;
							if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info">
										<?= $mail; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
