<?php

get_header();
$fields = get_fields();
$query = get_queried_object();
$posts = new WP_Query([
    'posts_per_page' => 9,
    'post_type' => 'place',
    'tax_query' => [
        [
            'taxonomy' => 'place_cat',
            'field' => 'term_id',
            'terms' => $query->term_id,
        ]
    ]
]);
$posts_all = get_posts([
	'numberposts' => -1,
	'post_type' => 'place',
	'tax_query' => [
		[
			'taxonomy' => 'place_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
?>
<article class="page-body places-page">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="base-output block-text">
					<h1><?= $query->name; ?></h1>
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($posts->posts as $x => $post) : ?>
					<?php get_template_part('views/partials/card', 'place', [
							'post' => $post,
					]); ?>
				<?php endforeach; ?>
			</div>
		<?php else: ?>
			<div class="row my-3">
				<div class="col-12">
					<h3 class="base-title text-center">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h3>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if (count($posts_all) > 9 && $posts->have_posts()) : ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="card-link load-more-link load-more-posts" data-type="place"
				data-term="<?= $query->term_id; ?>" data-tax-type="place_cat">טען עוד תוצאות</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
</article>
<div class="repeat-form-back">
	<?php get_template_part('views/partials/repeat', 'form',
			[
					'title' => get_field('res_form_title', $query),
					'subtitle' => get_field('res_form_subtitle', $query),
			]); ?>
</div>
<?php if ($slider = get_field('single_slider_seo', $query)) : ?>
	<div class="dark-slider">
		<?php get_template_part('views/partials/content', 'slider',
			[
					'img' => get_field('slider_img', $query),
					'content' => $slider,
			]); ?>
	</div>
<?php endif;
$same_posts_chosen = get_field('res_posts', $query);
$more_posts = $same_posts_chosen ? $same_posts_chosen : get_posts([
	'numberposts' => 3,
	'post_type' => 'post',
]); ?>
<section class="pt-pb">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="base-output block-text">
					<?php if ($text = get_field('res_posts_text', $query)) : ?>
						<?= $text; ?>
					<?php else : ?>
						<h2>קראו איתנו</h2>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="row justify-content-center align-items-stretch">
			<?php foreach ($more_posts as $post) {
				get_template_part('views/partials/card', 'post',
					[
						'post' => $post,
					]);
			} ?>
		</div>
		<?php if ($link = get_field('res_posts_link', $query)) : ?>
			<div class="row justify-content-end">
				<div class="col-auto">
					<a href="<?= $link['url']; ?>" class="base-link">
						<?= (isset($link['title']) && $link['title']) ?
							$link['title'] : 'למאמרים'; ?>
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>

<?php if ($faq = get_field('faq_item', $query)) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => get_field('faq_text', $query),
					'faq' => $faq,
			]);
endif;
get_footer(); ?>
