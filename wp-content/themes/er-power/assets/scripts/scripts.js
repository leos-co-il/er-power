(function($) {
	//Create select with cities
	$('#inputLoc').change(function() {
		var location = '';
		$( '#inputLoc option:selected').each(function() {
			location = $( this ).data('id');
		});
		var select = $('.form-group').find($('#inputCity'));
		$('#inputCity').children('option:not(:first)').remove();
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'post',
			dataType: 'json',
			data: {
				location: location,
				action: 'cities_search',
			},
			success: function (data) {
				var options = data.html;
				$.each(options, function (i, item) {
					select.append($('<option>', {
						value: i,
						text : item
					}));
				});
			}
		});
	});
	var selectReg = $('.select-region');
	selectReg.children('option:not(:first)').remove();
	var locs = $('.locations-hidden').html();
	selectReg.append(locs);
	$( document ).ready(function() {

		//Load more items in gallery
		var sizeLi = $('#members-block .card-member-col').size();
		var x = 3;
		$('#load-more-items-members').click(function () {
			x = (x + 3 <= sizeLi) ? x + 3 : sizeLi;
			if (x === sizeLi) {
				$('#load-more-items-members').addClass('hide');
			}
			$('#members-block .card-member-col:lt('+x+')').addClass('show');
		});
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.toggleClass( 'show-menu' );
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.pop-trigger-another').click(function () {
			$('.pop-form-another').addClass('show-popup');
			$('.float-form-another').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form-another').click(function () {
			$('.pop-form-another').removeClass('show-popup');
			$('.float-form-another').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.tel-trigger').click(function () {
			$('.tel-desktop-wrap').addClass('show-popup');
			$('.float-tel').addClass('show-float-tel');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-tel').click(function () {
			$('.tel-desktop-wrap').removeClass('show-popup');
			$('.float-tel').removeClass('show-float-tel');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.search-trigger').click(function () {
			$('.pop-form-search').addClass('show-popup');
			$('.float-form-search').addClass('show-float-tel');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-search').click(function () {
			$('.pop-form-search').removeClass('show-popup');
			$('.float-form-search').removeClass('show-float-tel');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-video').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
			show.parent().children('.question-title').addClass('active-faq');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
			collapsed.parent().children('.question-title').removeClass('active-faq');
		});
	});
	//More posts
	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		var params = $('.take-json').html();
		var postType = $(this).data('type');
		var taxType = $(this).data('tax-type');

		var ids = '';
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				ids: ids,
				taxType: taxType,
				params: params,
				action: 'get_more_function',
			},
			success: function (data) {
				console.log(data);
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
})( jQuery );
// var videoHome = $('#video-home');
// $('.video-button').click(function(){
// 	videoHome.paused ? this.play() : videoHome.get(0).pause();
// });
var v = document.getElementById('video-home');
var tr = document.getElementById('video-button');
if (tr && v) {
	tr.addEventListener(
		'play',
		function() {
			v.play();
		},
		false);

	tr.onclick = function() {
		if (v.paused) {
			v.play();
		} else {
			v.pause();
		}

		return false;
	};
}
