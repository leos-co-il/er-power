<?php
/*
Template Name: בתי אבות
*/

get_header();
$fields = get_fields();
$posts_all = get_posts([
		'numberposts' => -1,
		'post_type' => 'place',
]);
$_place_locations = (isset($_GET['place-location'])) ? $_GET['place-location'] : null;
$_place_cities = (isset($_GET['place-city'])) ? $_GET['place-city'] : null;
$_place_types = (isset($_GET['place-type'])) ? $_GET['place-type'] : null;
$_query = (isset($_GET['search-query'])) ? sanitize_text_field($_GET['search-query']) : null;
$query_args = [
		'post_type' => 'place',
		'posts_per_page' => 9,
];
if($_place_locations || $_place_cities || $_place_types){
	$query_args['tax_query'] = [
			'relation' => 'AND',
			$_place_locations ? [
					'taxonomy' => 'location',
					'field'    => 'term_id',
					'terms'    => [$_place_locations],
			] : null,
			$_place_cities ? [
					'taxonomy' => 'location',
					'field'    => 'term_id',
					'terms'    => [$_place_cities],
			] : null,
			$_place_types ? [
					'taxonomy' => 'place_cat',
					'field'    => 'term_id',
					'terms'    => $_place_types,
			] : null,
	];
}else{
	$query_args['tax_query'] = null;
}

if($_query){
	$query_args['s'] = $_query;
}
$posts = new WP_Query($query_args);
$json = json_encode($query_args);
?>
<article class="page-body places-page">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="base-output block-text">
					<?php if ($_place_locations || $_place_cities || $_place_types) :
						$place_cat_name = $_place_types ? get_term($_place_types)->name : '';
						$place_loc_name = $_place_locations ? get_term($_place_locations)->name : '';
						$place_city_name = $_place_cities ? get_term($_place_cities)->name : ''; ?>
						<h1><?= 'תוצאות חיפוש לבתי אבות '.$place_cat_name.' ב'.$place_loc_name; ?></h1>
					<?php else: ?>
					<h1><?php the_title(); ?></h1>
						<?php the_content(); endif; ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($posts->posts as $x => $post) : ?>
					<?php get_template_part('views/partials/card', 'place', [
							'post' => $post,
					]); ?>
				<?php endforeach; ?>
			</div>
		<?php else: ?>
			<div class="row my-3">
				<div class="col-12">
					<h3 class="base-title text-center">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h3>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if (count($posts_all) > 9 && $posts->have_posts()) : ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="card-link load-more-link load-more-posts" data-type="place">טען עוד תוצאות</div>
				<span class="take-json d-none"><?= $json; ?></span>
			</div>
		</div>
	</div>
	<?php endif; ?>
</article>
<div class="repeat-form-back">
	<?php get_template_part('views/partials/repeat', 'form',
		[
				'title' => $fields['res_form_title'],
				'subtitle' => $fields['res_form_subtitle'],
		]); ?>
</div>
<?php if ($fields['single_slider_seo']) : ?>
	<div class="dark-slider">
		<?php get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['slider_img'],
					'content' => $fields['single_slider_seo'],
			]); ?>
	</div>
<?php endif;
if ($fields['res_posts'] || $fields['res_posts_text']) : ?>
	<section class="pt-pb">
		<div class="container">
			<?php if ($fields['res_posts_text']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="base-output block-text">
							<?= $fields['res_posts_text']; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['res_posts'] as $post) {
					get_template_part('views/partials/card', 'post',
							[
									'post' => $post,
							]);
				} ?>
			</div>
			<?php if ($fields['res_posts_link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $fields['res_posts_link']['url']; ?>" class="base-link">
							<?= (isset($fields['res_posts_link']['title']) && $fields['res_posts_link']['title']) ?
									$fields['res_posts_link']['title'] : 'למאמרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>
