<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();

?>

<article class="page-body">
	<div class="cats-main-block" <?php if (has_post_thumbnail()) : ?>
		style="background-image: url('<?= postThumb(); ?>')"
	<?php endif; ?>>
		<?php get_template_part('views/partials/repeat', 'search'); ?>
		<div class="inside-overlay">
		</div>
	</div>
	<?php if ($fields['cat_regions_text'] || $fields['cat_regions']) {
		get_template_part('views/partials/content', 'locations',
				[
						'text' => $fields['cat_regions_text'],
						'cats' => $fields['cat_regions']
				]);
	} ?>
</article>
<?php
if ($fields['cat_places']) : ?>
	<section class="pt-pb">
		<div class="container">
			<?php if ($fields['cat_places_text']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="base-output block-text">
							<?= $fields['cat_places_text']; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['cat_places'] as $place) {
					get_template_part('views/partials/card', 'place',
							[
									'post' => $place,
							]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['cat_cats_text'] || $fields['cat_cats_chosen']) : ?>
<div class="cats-block-page">
	<?php get_template_part('views/partials/content', 'locations',
		[
			'text' => $fields['cat_cats_text'],
			'cats' => $fields['cat_cats_chosen']
		]); ?>
</div>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['slider_img'],
					'content' => $fields['single_slider_seo'],
			]);
}
get_footer(); ?>
