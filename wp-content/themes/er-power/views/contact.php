<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();
$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
?>

<article class="page-body contact-page-body" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
<?php endif; ?>>
	<div class="contact-page-overlay">
		<div class="overlay-gradient"></div>
		<div class="container z-index-more">
			<div class="row justify-content-center">
				<div class="col-xl-8 col-md-10 col-12">
					<div class="base-output block-text white-output">
						<h1><?php single_post_title(); ?></h1>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container z-index-more">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="contact-body-wrap">
						<div class="row align-items-center">
							<div class="col-lg-6 col-12">
								<div class="contact-column">
									<?php if ($tel) : ?>
										<div class="contact-item contact-item-link wow zoomIn" data-wow-delay="0.2s">
									<span class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-tel.png" alt="tel-icon">
									</span>
											<a href="tel:<?= $tel; ?>" class="contact-info">
												<?= $tel; ?>
											</a>
										</div>
									<?php endif;
									if ($mail) : ?>
										<div class="contact-item wow zoomIn" data-wow-delay="0.4s">
									<span class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-mail.png" alt="mail-icon">
									</span>
											<a href="mailto:<?= $mail; ?>" class="contact-info">
												<?= $mail; ?>
											</a>
										</div>
									<?php endif;
									if ($address) : ?>
										<div class="contact-item wow zoomIn" data-wow-delay="0.6s">
									<span class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-geo.png" alt="geo-icon">
									</span>
											<a href="https://www.waze.com/ul?q=<?= $address; ?>" class="contact-info">
												<?= $address; ?>
											</a>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<div class="col-lg-6 col-12 mt-sm-0 mt-4">
								<div class="dotted-form contact-form">
									<?php if ($fields['contact_form_title']) : ?>
										<h2 class="pop-form-title text-right"><?= $fields['contact_form_title']; ?></h2>
									<?php endif;
									getForm('19'); ?>
								</div>
								<?php if ($map = opt('map_image')) : ?>
									<a href="<?= $map['url']; ?>" class="w-100 d-flex my-3" data-lightbox="map">
										<img src="<?= $map['url']; ?>" alt="map" class="w-100">
									</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
