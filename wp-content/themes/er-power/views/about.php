<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
$count = '';
?>

<article class="page-body about-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['about_team_item'] || $fields['about_team_text']) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-5 col-lg-6 col-md-8 col-sm-10 col-12">
					<div class="base-output text-center mb-5">
						<?= $fields['about_team_text']; ?>
					</div>
				</div>
			</div>
			<?php if ($fields['about_team_item']) : $count = count($fields['about_team_item']); ?>
				<div class="row justify-content-center align-items-stretch" id="members-block">
					<?php foreach ($fields['about_team_item'] as $x => $member) : ?>
						<div class="col-lg-4 col-md-6 col-sm-10 col-12 card-member-col <?= ($x < 6) ? 'show' : ''; ?>">
							<div class="post-home-item">
								<div class="post-item-img home-item-img" <?php if ($member['member_img']) : ?>
									style="background-image: url('<?= $member['member_img']['url']; ?>')" <?php endif; ?>>
								</div>
								<div class="home-item-content">
									<div class="flex-grow-1 d-flex flex-column align-items-center">
										<h3 class="home-item-title"><?= $member['member_name']; ?></h3>
										<?php if ($member['member_position']) : ?>
											<h4 class="home-item-small-desc"><?= $member['member_position']; ?></h4>
										<?php endif; ?>
										<p class="home-item-desc mb-3">
											<?= text_preview($member['member_text'], 14); ?>
										</p>
										<div class="rev-pop-trigger">
											<div class="hidden-review">
												<div class="base-output">
													<?= $member['member_text']; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
					 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<i class="fas fa-times"></i>
							</button>
							<div class="modal-body" id="reviews-pop-wrapper"></div>
						</div>
					</div>
				</div>
			<?php endif;
			if ($count > 6) : ?>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-auto">
							<a class="card-link load-more-link load-more" id="load-more-items-members">
								<span>טען עוד אנשי צוות</span>
							</a>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</article>
<div class="container">
	<div class="row">
		<div class="col-12">
		</div>
	</div>
</div>


<?php get_footer(); ?>
