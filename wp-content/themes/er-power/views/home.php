<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
$logo = opt('logo');
?>

<section class="home-video-block">
	<?php if ($fields['home_video_back']) : ?>
		<video muted  id="video-home">
			<source src="<?= $fields['home_video_back']['url']; ?>" type="video/mp4">
		</video>
	<?php endif; ?>
	<div class="contact-page-overlay">
		<div class="overlay-gradient"></div>
	</div>
	<div class="home-content z-index-more">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-11 col-12">
					<div class="row">
						<div class="col-xl-8 col-lg-10 col-12">
							<?php if ($fields['home_main_text']) : ?>
								<p class="home-text-white"><?= $fields['home_main_text']; ?></p>
							<?php endif; ?>
						</div>
					</div>
					<span id="video-button" class="button-home">
						<img src="<?= ICONS ?>play-home.png" alt="play-video">
					</span>
				</div>
			</div>
		</div>
		<?php if ($fields['home_main_cats'] || $fields['home_main_title']) : ?>
			<div class="home-cats-main">
				<div class="container">
					<?php if ($fields['home_main_title']) : ?>
						<div class="row justify-content-center">
							<div class="col-12">
								<h2 class="giant-title"><?= $fields['home_main_title']; ?></h2>
							</div>
						</div>
					<?php endif; ?>
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['home_main_cats'] as $y => $cat_home) : ?>
							<div class="col-lg-4 col-sm-6 col-12 wow fadeIn home-cat-col-main" data-wow-delay="0.<?= $y + 1; ?>s">
								<a class="home-category-item" href="<?= get_term_link($cat_home); ?>">
									<?= $cat_home->name; ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif;
		get_template_part('views/partials/repeat', 'search');
		if ($fields['home_main_link']) : ?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-auto mt-3">
						<a href="<?= $fields['home_main_link']['url']; ?>" class="base-link">
							<?= (isset($fields['home_main_link']['title']) && $fields['home_main_link']['title']) ?
									$fields['home_main_link']['title'] : 'מעבר לפוסט של הסרטון'; ?>
						</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php if ($fields['home_places_chosen_1'] || $fields['home_places_text_1']) : ?>
	<section class="pt-pb">
		<div class="container">
			<?php if ($fields['home_places_text_1']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="base-output block-text">
							<?= $fields['home_places_text_1']; ?>
						</div>
					</div>
				</div>
			<?php endif;
			if ($fields['home_places_chosen_1']) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['home_places_chosen_1'] as $place) {
						get_template_part('views/partials/card', 'place',
								[
										'post' => $place,
								]);
					} ?>
				</div>
			<?php endif;
			if ($fields['home_places_link_1']) {
				get_template_part('views/partials/part', 'link_places',
						[
								'link' => $fields['home_places_link_1'],
						]);
			} ?>
		</div>
	</section>
<?php endif; ?>
<section class="home-about-block" <?php if ($fields['home_about_back']) : ?>
	style="background-image: url('<?= $fields['home_about_back']['url']; ?>')"
<?php endif; ?>>
	<?php if ($fields['home_about_text'] || $logo) : ?>
		<div class="container about-content-container">
			<div class="row justify-content-between align-items-center">
				<?php if ($fields['home_about_text'] || $fields['home_about_link']) : ?>
					<div class="col-xl-7 col-md-8 col-12 d-flex flex-column justify-content-center align-items-start">
						<div class="base-output white-output">
							<?= $fields['home_about_text']; ?>
						</div>
						<?php if ($fields['home_about_link']) : ?>
							<a href="<?= $fields['home_about_link']['url']; ?>" class="base-link">
								<?= (isset($fields['home_about_link']['title']) && $fields['home_about_link']['title']) ?
										$fields['home_about_link']['title'] : 'עוד עלינו'; ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endif;
				if ($logo) : ?>
					<div class="col-xl-4 col-md-3 col-6">
						<a href="/" class="logo">
							<img src="<?= $logo['url']; ?>" alt="logo">
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif;
	get_template_part('views/partials/repeat', 'form',
			[
					'title' => $fields['h_about_form_title'],
					'subtitle' => $fields['h_about_form_subtitle']
			]);
	if ($fields['home_regions_text'] || $fields['home_regions_chosen']) {
		get_template_part('views/partials/content', 'locations',
				[
						'text' => $fields['home_regions_text'],
						'cats' => $fields['home_regions_chosen']
				]);
	} ?>
</section>
<?php if ($fields['h_single_slider_seo']) : ?>
	<div class="dark-slider">
		<?php get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['h_slider_img'],
					'content' => $fields['h_single_slider_seo'],
			]); ?>
	</div>
<?php endif;
if ($fields['home_places_chosen_2']) : ?>
<section class="pt-pb">
	<div class="container">
		<?php if ($fields['home_places_text_2']) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="base-output block-text">
						<?= $fields['home_places_text_2']; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center align-items-stretch">
			<?php foreach ($fields['home_places_chosen_2'] as $place) {
				get_template_part('views/partials/card', 'place',
						[
								'post' => $place,
						]);
			} ?>
		</div>
		<?php if ($fields['home_places_link_2']) {
			get_template_part('views/partials/part', 'link_places',
					[
							'link' => $fields['home_places_link_2'],
					]);
		} ?>
	</div>
</section>
<?php endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
			]);
endif;
if ($fields['home_posts_chosen'] || $fields['home_posts_text']) : ?>
	<section class="pt-pb">
		<div class="container">
			<?php if ($fields['home_posts_text']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="base-output block-text">
							<?= $fields['home_posts_text']; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['home_posts_chosen'] as $post) {
					get_template_part('views/partials/card', 'post',
							[
									'post' => $post,
							]);
				} ?>
			</div>
			<?php if ($fields['home_posts_link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $fields['home_posts_link']['url']; ?>" class="base-link">
							<?= (isset($fields['home_posts_link']['title']) && $fields['home_posts_link']['title']) ?
									$fields['home_posts_link']['title'] : 'למאמרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['slider_img'],
					'content' => $fields['single_slider_seo'],
			]);
}
get_footer(); ?>
