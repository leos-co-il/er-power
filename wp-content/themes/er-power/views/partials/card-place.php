<?php if(isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
<div class="col-lg-4 col-md-6 col-sm-10 col-12 card-home-col">
	<div class="post-home-item more-card" data-id="<?= $args['post']->ID; ?>">
		<div class="post-item-img home-item-img" <?php if (has_post_thumbnail($args['post'])) : ?>
			style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>>
			<?php if ($tel = get_field('place_tel', $args['post'])) : ?>
				<a class="card-link home-item-tel" href="tel:<?= $tel; ?>">
					<?= $tel; ?>
					<img src="<?= ICONS ?>item-tel.png" alt="tel">
				</a>
			<?php endif; ?>
			<div class="home-item-cats-wrap">
				<?php $cats_place = wp_get_object_terms($args['post']->ID, 'place_cat');
					$cats_loc = wp_get_object_terms($args['post']->ID, 'location');
					foreach ($cats_place as $cat) : if ($cat->parent === 0) : ?>
						<a class="card-link link-cat" href="<?= get_category_link($cat); ?>">
							<?= get_term($cat)->name; ?>
						</a>
					<?php endif; endforeach;
					foreach ($cats_loc as $cat_loc) : if ($cat_loc->parent === 0) : ?>
						<a class="card-link link-cat-loc" href="<?= get_category_link($cat_loc); ?>">
							<?= get_term($cat_loc)->name; ?>
						</a>
					<?php endif; endforeach; ?>
			</div>
		</div>
		<div class="home-item-content">
			<div class="flex-grow-1">
				<h3 class="home-item-title"><?= $args['post']->post_title; ?></h3>
				<?php if ($subtitle = get_field('place_subtitle', $args['post'])) : ?>
					<h4 class="home-item-small-desc"><?= $subtitle; ?></h4>
				<?php endif; ?>
				<p class="home-item-desc mb-3">
					<?= text_preview($args['post']->post_content, 14); ?>
				</p>
			</div>
			<a href="<?php the_permalink($args['post']); ?>" class="card-link">
				קרא עוד
			</a>
		</div>
	</div>
</div>
<?php endif; ?>
