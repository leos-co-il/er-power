<div class="repeat-form">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="foo-form-wrap">
					<div class="row align-items-center justify-content-center mb-3">
						<div class="col-auto">
							<h2 class="form-title"><?= (isset($args['title']) && $args['title']) ? $args['title'] :
									opt('base_form_title'); ?>
							</h2>
						</div>
						<div class="col-auto">
							<h2 class="form-subtitle"><?= (isset($args['subtitle']) && $args['subtitle']) ? $args['subtitle'] :
									opt('base_form_subtitle'); ?>
							</h2>
						</div>
					</div>
					<?php getForm('15'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
