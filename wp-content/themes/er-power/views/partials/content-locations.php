<div class="cats-locations-block">
	<div class="container">
		<?php if(isset($args['text']) && $args['text']) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="base-output block-text">
						<?= $args['text']; ?>
					</div>
				</div>
			</div>
		<?php endif;
		if(isset($args['cats']) && $args['cats']) : ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($args['cats'] as $cat) : ?>
					<div class="col-md-4 col-12 loc-col-wrap">
						<a href="<?= $link = get_term_link($cat); ?>" class="location-wrap-item h-100">
							<span class="location-wrap-inside"><?= $cat->name; ?></span>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>
