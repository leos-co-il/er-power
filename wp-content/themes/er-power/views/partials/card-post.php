<?php if(isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-4 col-md-6 col-sm-10 col-12 card-home-col post-item-col">
		<div class="post-home-item more-card" data-id="<?= $args['post']->ID; ?>">
			<div class="post-item-img" <?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>>
			</div>
			<div class="home-item-content">
				<div class="flex-grow-1">
					<h3 class="home-item-title mb-2"><?= $args['post']->post_title; ?></h3>
					<p class="home-item-desc mb-2">
						<?= text_preview($args['post']->post_content, 14); ?>
					</p>
				</div>
				<a href="<?php the_permalink($args['post']); ?>" class="card-link">
					המשך קריאה
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
