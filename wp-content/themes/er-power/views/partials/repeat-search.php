<?php
$place_types = get_terms([
	'taxonomy' => 'place_cat',
	'hide_empty' => false,
]);
$locations = get_terms([
	'taxonomy' => 'location',
	'hide_empty' => false,
	'parent' => 0,
]);
$search_block_text = opt('search_text');
$page = opt('search_page');
$page_2 = get_the_permalink(getPageByTemplate('views/places.php'));
?>

<div class="main-search-section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="search-wrap">
					<?php if ($search_block_text) : ?>
						<div class="base-output block-text white-output mb-4">
							<?= $search_block_text; ?>
						</div>
					<?php endif; ?>
					<form method="get" action="<?= $page ? $page['url'] : $page_2; ?>">
						<div class="row align-items-stretch">
							<?php if ($place_types): ?>
								<div class="form-group col-xl col-lg-4 col-12">
									<select id="inputTypePlace" name="place-type" class="form-control-input">
										<option selected disabled><?= esc_html__('סוג המוסד','leos')  ?></option>
										<?php foreach($place_types as $place_type): ?>
											<option value="<?= $place_type->term_id ?>"><?= $place_type->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif;
							if ($locations): ?>
								<div class="form-group col-xl col-lg-4 col-sm-6 col-12">
									<select id="inputLoc" name="place-location" class="form-control-input">
										<option selected disabled><?= esc_html__('אזור','leos')  ?></option>
										<?php foreach($locations as $location): ?>
											<option value="<?= $location->term_id ?>" data-id="<?= $location->term_id ?>"><?= $location->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif; ?>
							<div class="form-group col-xl col-lg-4 col-sm-6 col-12">
								<select id="inputCity" name="place-city" class="form-control-input">
									<option selected disabled><?= esc_html__('עיר','leos')  ?></option>
								</select>
							</div>
							<div class="form-group col-xl col-12">
								<button type="submit" class="btn-search"><?= esc_html__('מצאו את הדיור בשבילכם','leos') ?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
