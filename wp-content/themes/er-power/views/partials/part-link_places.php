<?php if (isset($args['link']) && $args['link']) : ?>
<div class="row justify-content-end">
	<div class="col-auto">
		<a href="<?= $args['link']['url']; ?>" class="base-link">
			<?= (isset($args['link']['title']) && $args['link']['title']) ?
				$args['link']['title'] : 'לכל בתי האבות'; ?>
		</a>
	</div>
</div>
<?php endif; ?>
