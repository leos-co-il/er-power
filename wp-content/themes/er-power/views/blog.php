<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts_all = get_posts([
		'numberposts' => -1,
		'post_type' => 'post',
]);
$query_args = [
		'post_type' => 'post',
		'posts_per_page' => 9,
];
$posts = new WP_Query($query_args);
?>
<article class="page-body places-page">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="base-output block-text">
					<h1><?php the_title(); ?></h1>
					<?php the_content();  ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($posts->posts as $x => $post) : ?>
					<?php get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]); ?>
				<?php endforeach; ?>
			</div>
		<?php else: ?>
			<div class="row my-3">
				<div class="col-12">
					<h3 class="base-title text-center">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h3>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if (count($posts_all) > 9 && $posts->have_posts()) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="card-link load-more-link load-more-posts" data-type="post">טען עוד מאמרים</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<div class="repeat-form-back">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php if ($fields['single_slider_seo']) : ?>
	<div class="dark-slider">
		<?php get_template_part('views/partials/content', 'slider',
				[
						'img' => $fields['slider_img'],
						'content' => $fields['single_slider_seo'],
				]); ?>
	</div>
<?php endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>
