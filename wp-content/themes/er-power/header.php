<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif;
$tel = opt('tel');
$regions = get_terms([
		'taxonomy' => 'location',
		'hide_empty' => false,
		'parent' => 0
]); ?>
<div class="locations-hidden">
	<option class="option-loc" disabled>אוזר</option>
	<?php foreach ($regions as $opt) : ?>
		<option value="<?= $opt->name; ?>" class="option-loc"><?= $opt->name; ?></option>
	<?php endforeach; ?>
</div>


<header>
	<div class="header-top">
		<div class="container-fluid">
			<div class="row align-items-center">
				<?php if ($logo = opt('logo')) : ?>
					<div class="col-auto">
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				<?php endif; ?>
				<div class="col d-flex align-items-stretch justify-content-start menu-col-main">
					<nav id="MainNav" class="h-100">
						<div id="MobNavBtn">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
					</nav>
					<div class="pop-trigger-another">  הצטרפות כבית אבות</div>
				</div>
				<?php if ($tel) : ?>
					<div class="col-auto">
						<a href="tel:<?= $tel; ?>" class="header-tel-wrap">
							<span class="tel-text tel-hide-text">יעוץ מיידי :</span>
							<span class="tel-text d-flex align-items-center">
								<?= $tel;?>
								<img src="<?= ICONS ?>header-tel.png" alt="tel-icon">
							</span>
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if ($messages = opt('messages')) : ?>
		<div class="top-events main-ticker-wrap">
			<div class="msg-ticker-wrap" style="animation-duration: 10s;">
				<?php foreach ($messages as $msg): ?>
					<div class="msg-item">
						<a href="<?= $msg['msg_link']; ?>" class="event-link" style="direction: rtl;">
							<span class="base-large-text white-text font-weight-normal">
								<?= $msg['msg_title'].' | ' ?>
							</span>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</header>

<div class="triggers-wrap">
	<div class="trigger-item pop-trigger">
		<img src="<?= ICONS ?>pop-trigger.png" alt="pop-trigger">
	</div>
	<?php if ($facebook = opt('facebook')) : ?>
		<a class="trigger-item facebook-fix" href="<?= $facebook; ?>">
			<i class="fab fa-facebook-f"></i>
		</a>
	<?php endif;
	if ($whatsapp = opt('whatsapp')) : ?>
		<a class="trigger-item whatsapp-fix" href="<?= $whatsapp; ?>">
			<i class="fab fa-whatsapp"></i>
		</a>
	<?php endif;
	if ($facebook) : ?>
		<a href="https://m.me/<?= $facebook; ?>" class="trigger-item messenger-fix">
			<i class="fab fa-facebook-messenger"></i>
		</a>
	<?php endif; ?>
	<div class="trigger-item tel-trigger">
		<img src="<?= ICONS ?>tel-trigger.png" alt="tel-trigger">
	</div>
	<div class="trigger-item search-trigger">
		<img src="<?= ICONS ?>search-trigger.png" alt="search-trigger">
	</div>
</div>
<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form dotted-form pb-3">
					<div class="form-col">
						<span class="close-form">
							<?= svg_simple(ICONS.'close.svg'); ?>
						</span>
						<?php if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="pop-form-title"><?= $f_title; ?></h2>
						<?php endif;
						getForm('20'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pop-form-another">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form-another dotted-form pb-3">
					<div class="form-col">
						<span class="close-form-another">
							<?= svg_simple(ICONS.'close.svg'); ?>
						</span>
						<?php if ($f_title = opt('pop_form_title_another')) : ?>
							<h2 class="pop-form-title"><?= $f_title; ?></h2>
						<?php endif;
						getForm('18'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php if ($tel) : ?>
<section class="tel-desktop-wrap">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto d-flex justify-content-center">
				<div class="float-tel dotted-form">
					<span class="close-tel">
						<?= svg_simple(ICONS.'close.svg'); ?>
					</span>
					<a class="tel-pop-line" href="tel:<?= $tel; ?>">
						<span class="text-hide">חייגו אלינו למספר - </span>
						<span><?= ' '.$tel;?></span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;
$page = opt('search_page');
$page_2 = get_the_permalink(getPageByTemplate('views/places.php'));
?>

<section class="pop-form-search">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form-search dotted-form pb-3">
					<div class="form-col">
						<span class="close-search">
							<?= svg_simple(ICONS.'close.svg'); ?>
						</span>
						<form role="search" method="get" class="search-form" action="<?= $page ? $page['url'] : $page_2; ?>">
							<input id="search-input" type="search" class="search-input" value="" name="search-query" title="<?= esc_html__('חפש באתר','leos') ?>" />
							<input type="submit" class="search-submit" value="חפש" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
