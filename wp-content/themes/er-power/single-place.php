<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'place_cat', ['fields' => 'ids']);
$post_locs = wp_get_object_terms($postId, 'location', ['fields' => 'ids']);
?>
<article class="page-body post-body">
	<div class="container">
		<div class="row justify-content-between align-items-stretch mt-5">
			<div class="col-12">
				<h1 class="post-title-page text-center"><?php the_title(); ?></h1>
			</div>
			<?php if ($post_terms || $post_locs) : ?>
				<div class="col-12">
					<div class="place-page-cats row justify-content-center align-items-stretch mb-5">
						<?php foreach ($post_terms as $cat) : ?>
							<div class="col-auto">
								<a class="card-link link-cat" href="<?= get_category_link($cat); ?>">
									<?= get_term($cat)->name; ?>
								</a>
							</div>
						<?php endforeach;
						foreach ($post_locs as $cat_loc) : ?>
							<div class="col-auto">
								<a class="card-link link-cat-loc" href="<?= get_category_link($cat_loc); ?>">
									<?= get_term($cat_loc)->name; ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
			<div class="col-lg-6 col-12 col-content">
				<?php if($fields['place_location']) : ?>
					<div class="contact-item-place">
						<img src="<?= ICONS ?>geo-place.png" alt="geo-icon">
						<a href="https://www.waze.com/ul?q=<?= $fields['place_location']; ?>" class="contact-info-place">
							<?= $fields['place_location']; ?>
						</a>
					</div>
				<?php endif;
				if($fields['place_tel']) : ?>
					<div class="contact-item-place">
						<img src="<?= ICONS ?>tel-place.png" alt="tel-icon">
						<a href="tel:<?= $fields['place_tel']; ?>" class="contact-info-place">
							<?= $fields['place_tel']; ?>
						</a>
					</div>
				<?php endif;
				if($fields['place_mail']) : ?>
					<div class="contact-item-place">
						<img src="<?= ICONS ?>mail-place.png" alt="maill-icon">
						<a href="mailto:<?= $fields['place_mail']; ?>" class="contact-info-place">
							<?= $fields['place_mail']; ?>
						</a>
					</div>
				<?php endif; ?>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-lg-6 col-12">
				<div class="gallery-slider">
					<div class="base-slider" dir="rtl">
						<div>
							<?php if (has_post_thumbnail()) : ?>
								<img src="<?= postThumb(); ?>" alt="post-image" class="gallery-img">
							<?php endif; ?>
						</div>
						<?php if ($fields['place_gallery_slider']) : foreach ($fields['place_gallery_slider'] as $img) : ?>
							<div>
								<img src="<?= $img['url']; ?>" alt="post-image" class="gallery-img">
							</div>
						<?php endforeach; endif; ?>
					</div>
				</div>
				<?php if ($fields['place_video_link']) : ?>
					<div class="post-video">
						<img src="<?= getYoutubeThumb($fields['place_video_link']); ?>" class="video-image" alt="video-image">
						<div class="play-button-wrap">
							<span class="play-video play-button" data-video="<?= getYoutubeId($fields['place_video_link']); ?>">
								<img src="<?= ICONS ?>white-arrow-right.png" alt="play-video">
							</span>
						</div>
					</div>
				<?php endif; ?>
				<div class="video-modal">
					<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
						 aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-body" id="iframe-wrapper"></div>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true" class="close-icon">×</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<div class="form-search-back">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 3,
	'post_type' => 'place',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'place_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_places']) {
	$samePosts = $fields['same_places'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'post_type' => 'place',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="posts-output pt-pb">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title-dark">
						<?= $fields['same_title'] ? $fields['same_title'] : 'בתי אבות שאולי יעניינו אתכם'; ?>
					</h2>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<?php foreach ($samePosts as $x => $post) {
					get_template_part('/views/partials/card', 'place', [
						'post' => $post,
					]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
get_footer(); ?>


